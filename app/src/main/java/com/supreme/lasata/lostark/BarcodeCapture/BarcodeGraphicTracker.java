package com.supreme.lasata.lostark.BarcodeCapture;

/**
 * Created by navee on 01/01/2017.
 */

import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.supreme.lasata.lostark.BarcodeCapture.camera.GraphicOverlay;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by lasata on 18/12/2016.
 */
public class BarcodeGraphicTracker extends Tracker<Barcode> {
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private BarcodeGraphic mGraphic;
    private Set<String> collectionOfBarcodes;

    public BarcodeGraphicTracker(GraphicOverlay<BarcodeGraphic> graphicOverlay, BarcodeGraphic barcodeGraphic) {
        mGraphicOverlay = graphicOverlay;
        mGraphic = barcodeGraphic;
        this.collectionOfBarcodes = new HashSet<String>();
    }

    @Override
    public void onNewItem(int i, Barcode barcode) {
        mGraphic.setmId(i);
    }

    @Override
    public void onUpdate(Detector.Detections<Barcode> detectionResults, Barcode barcode) {
        Log.e("Tracker" , "Receiced Barcodes number: " + detectionResults.getDetectedItems().size());
        mGraphicOverlay.add(mGraphic);
        mGraphic.updateItem(barcode);
        final SparseArray<Barcode> listOfBarcodes = detectionResults.getDetectedItems();
        for (int i =0; i < listOfBarcodes.size(); i++) {
            collectionOfBarcodes.add(listOfBarcodes.valueAt(i).displayValue);
            Log.e("Tracker" , "Details: " + listOfBarcodes.valueAt(0).displayValue);
        }

    }

    public Set<String> getListOfBarcodes() {
        return collectionOfBarcodes;
    }
    @Override
    public void onMissing(Detector.Detections<Barcode> detections) {
        mGraphicOverlay.remove(mGraphic);
    }

    @Override
    public void onDone() {
        mGraphicOverlay.remove(mGraphic);
    }
}
