package com.supreme.lasata.lostark.model;

/**
 * Created by navee on 01/01/2017.
 */

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by navee on 31/12/2016.
 */

public class JSONParser {

    private static final String TAG = "BOOKMANAGER";
    private static JSONParser instance = null;
    public static synchronized JSONParser getInstance() {
        if ( instance == null) {
            instance = new JSONParser();
        }
        return instance;
    }

    public Book parseJSONtoBook(JSONObject jsonObject) {
        Book book = null;
        Log.d(TAG, jsonObject.toString());
        try {
            int numberOfItemsAvailable = jsonObject.getInt("totalItems");
            if (numberOfItemsAvailable > 0) {
                book = new Book();
                JSONArray bookDataArray = jsonObject.getJSONArray("items");
                JSONObject bookDataObject = (JSONObject) bookDataArray.get(0);

                JSONObject bookVolumeInfoObj = bookDataObject.getJSONObject("volumeInfo");

                book.setName(bookVolumeInfoObj.getString("title"));
                book.setDescription(bookVolumeInfoObj.getString("description"));

                List<String> authorList = new ArrayList<String>();
                JSONArray authorArr = bookVolumeInfoObj.getJSONArray("authors");
                for (int i =0; i < authorArr.length(); i++) {
                    authorList.add(authorArr.getString(i));
                }
                book.setAuthorNames(authorList);

                JSONObject bookCoverImageUrsObj = bookVolumeInfoObj.getJSONObject("imageLinks");
                book.setBookCover(bookCoverImageUrsObj.getString("smallThumbnail"));

                JSONArray isbnArrayObj = bookVolumeInfoObj.getJSONArray("industryIdentifiers");
                book.setISBN(isbnArrayObj.getJSONObject(0).getString("identifier"));
            }
        } catch (JSONException e) {
            Log.e(TAG, "JSONException occured while parsing " + jsonObject );
            Log.e(TAG, e.toString());
        }
        return book;
    }


}

