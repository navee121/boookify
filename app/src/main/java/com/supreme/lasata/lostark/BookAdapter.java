package com.supreme.lasata.lostark;

/**
 * Created by navee on 01/01/2017.
 */


import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.supreme.lasata.lostark.model.Book;

import java.util.List;


/**
 * Created by lasata on 28/12/2016.
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyViewHolder> {

    private Context context;
    private List<Book> bookList;

    public BookAdapter(Context context, List<Book> bookList) {
        this.context = context;
        this.bookList = bookList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_card, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Book book = bookList.get(position);
        holder.title.setText(book.getName());
        Glide.with(context).load(book.getBookCover()).placeholder(R.drawable.defbookcover).into(holder.bookCover);
        holder.overflow.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                showPopUpMenu(holder.overflow);
            }
        });

    }

    private void showPopUpMenu(View overflow) {
        PopupMenu popupMenu = new PopupMenu(context, overflow);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.menu_book, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new MyMenuClickListener());
        popupMenu.show();
    }

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView bookCover, overflow;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.bookCover = (ImageView) itemView.findViewById(R.id.bookCover);
            this.overflow = (ImageView) itemView.findViewById(R.id.overflow);
        }


    }

    private class MyMenuClickListener implements PopupMenu.OnMenuItemClickListener {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.action_mark_read:
                    Toast.makeText(context, "marked as Read", Toast.LENGTH_LONG).show();
                    return true;
                case R.id.action_delete:
                    Toast.makeText(context, "Deleted", Toast.LENGTH_LONG).show();
                    return true;
                default:
            }
            return false;
        }
    }
}
