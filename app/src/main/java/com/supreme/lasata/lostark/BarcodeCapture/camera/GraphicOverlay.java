package com.supreme.lasata.lostark.BarcodeCapture.camera;

/**
 * Created by navee on 01/01/2017.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

/**
 * Created by lasata on 18/12/2016.
 */
public class GraphicOverlay<T extends GraphicOverlay.Graphic> extends View {
    private float widthScaleFactor = 1.0f;
    private float heightScaleFactor = 1.0f;
    private int facing = CameraSource.CAMERA_FACING_BACK;
    private int previewWidth;
    private int previewHeight;
    private final Object mLock = new Object();
    private Set<T> graphics = new HashSet<>();

    public GraphicOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static abstract class Graphic {
        private GraphicOverlay overLay;

        public Graphic(GraphicOverlay overLay) {
            this.overLay = overLay;
        }

        public abstract void draw(Canvas canvas);
        public float scaleX(float horizontal ) {
            return horizontal * overLay.widthScaleFactor;
        }
        public float scaleY (float verticle) {
            return verticle * overLay.heightScaleFactor;
        }

        public float translateX(float x) {
            if (overLay.facing == CameraSource.CAMERA_FACING_FRONT) {
                return overLay.getWidth() - scaleX(x);
            } else {
                return scaleX(x);
            }
        }

        public float translateY(float y) {
            return scaleY(y);
        }

        public void postInValidate() {
            overLay.postInvalidate();
        }
    }
    /*
    Remove all graphics from overlay
     */

    public void clear() {
        synchronized (mLock) {
            graphics.clear();
        }
    }

    public void add (T graphic) {
        synchronized (mLock) {
            graphics.add(graphic);
        }
        postInvalidate();
    }

    public void remove(T graphic) {
        synchronized (mLock) {
            graphics.remove(graphic);
        }
        postInvalidate();
    }

    public List<T> getGraphics() {
        synchronized (mLock) {
            return new Vector<>(graphics);
        }
    }

    public float getWidthScaleFactor() {
        return widthScaleFactor;
    }

    public float getHeightScaleFactor() {
        return heightScaleFactor;
    }

    public  void setCameraInfo (int previewWidth, int previewHeight, int facing) {
        synchronized (mLock) {
            this.previewWidth = previewWidth;
            this.previewHeight = previewHeight;
            this.facing = facing;
        }
        postInvalidate();
    }
    /*
    Draws the overlay with its associated graphic objects
     */

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        synchronized (mLock) {
            if ((previewHeight != 0) && (previewWidth != 0)) {
                widthScaleFactor = (float) canvas.getWidth() / (float) previewWidth;
                heightScaleFactor = (float) canvas.getHeight() / (float) previewHeight;
            }

            for (Graphic graphic : graphics) {
                graphic.draw(canvas);
            }
        }
    }
}
