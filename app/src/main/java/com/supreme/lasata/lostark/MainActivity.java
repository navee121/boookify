package com.supreme.lasata.lostark;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.supreme.lasata.lostark.BarcodeCapture.BarcodeCaptureActivity;
import com.supreme.lasata.lostark.model.Book;
import com.supreme.lasata.lostark.model.CustomResponseListener;
import com.supreme.lasata.lostark.model.JSONParser;
import com.supreme.lasata.lostark.model.NetworkManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private BookAdapter bookAdapter;
    private List<Book> bookList;
    private FloatingActionButton cameraButton;

    private static final int RC_BARCODE_CAPTURE = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        NetworkManager.getInstance(this);
        DataBaseHandler.getInstance(this);
        initCollapsingToolBar();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(bookAdapter);
        retrieveBookFromDb();
        Log.e(TAG, bookList.toString());

        try {
            Glide.with(this).load(R.drawable.hp)
                    .placeholder(R.drawable.defbookcover)
                    .into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }

        initiateCameraButton();
    }

    private void initiateCameraButton() {
        cameraButton = (FloatingActionButton) findViewById(R.id.openCamera);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCameraView();
            }
        });
    }

    private void openCameraView() {
        Intent intent = new Intent(MainActivity.this, BarcodeCaptureActivity.class);
        startActivityForResult(intent, RC_BARCODE_CAPTURE);
    }

    private void retrieveBookFromDb() {
        bookList = new ArrayList<Book>();
        bookList.addAll(DataBaseHandler.getInstance().getAllBooks());
        System.out.println("Retrived all books: " + bookList);
        BookAdapter bookAdapter = new BookAdapter(this, bookList);
        recyclerView.setAdapter(bookAdapter);
        recyclerView.invalidate();
        bookAdapter.notifyDataSetChanged();
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private void initCollapsingToolBar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if(resultCode == CommonStatusCodes.SUCCESS ){
                if (data != null) {
                    HashSet<String> barcodes = (HashSet<String>) data.getSerializableExtra(BarcodeCaptureActivity.BarcodeObject);
                    try {
                        loadBooks(barcodes);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, "MainAct: Barcode value is: " + barcodes);

                } else {
                    Log.d(TAG, "No barcode captured. Data is null");
                }
            } else {
                Log.e(TAG, getString(R.string.barcode_error));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void loadBooks(HashSet<String> barcodes) throws InterruptedException {
        for(final String barcode : barcodes) {
            NetworkManager.getInstance().executeJsonRequest(barcode, new CustomResponseListener<JSONObject>() {
                @Override
                public synchronized void getResult(JSONObject result) {
                    if(result != null ) {
                        addBookToDB(JSONParser.getInstance().parseJSONtoBook(result), barcode);
                    }
                }
            });
        }

    }
    private void addBookToDB(Book book, String isbn) {
        if (book != null) {
            System.out.println("Adding book to db: " + book);
            DataBaseHandler.getInstance().addBook(book);
            retrieveBookFromDb();
        } else {
            Toast.makeText(this, "Could not find any book with ISBN " + isbn, Toast.LENGTH_SHORT).show();
        }
    }
}
