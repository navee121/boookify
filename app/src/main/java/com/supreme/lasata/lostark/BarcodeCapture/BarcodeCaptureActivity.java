package com.supreme.lasata.lostark.BarcodeCapture;

/**
 * Created by navee on 01/01/2017.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.supreme.lasata.lostark.BarcodeCapture.camera.CameraSource;
import com.supreme.lasata.lostark.BarcodeCapture.camera.CameraSourcePreview;
import com.supreme.lasata.lostark.BarcodeCapture.camera.GraphicOverlay;
import com.supreme.lasata.lostark.R;

import java.io.IOException;
import java.util.HashSet;

/**
 * Created by navee on 31/12/2016.
 */

public class BarcodeCaptureActivity extends AppCompatActivity {
    //    public static final String AUTOFOCUS = "autoFocus";
//    public static final String USEFLASH = "useFlash";
    public static final String BarcodeObject = "Barcode";

    private HashSet<String> collectionOfBarcodes;
    private static final String TAG = "Barcode-reader";

    // intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    private CameraSource mCameraSource;
    private CameraSourcePreview cameraSourcePreview;
    private GraphicOverlay<BarcodeGraphic> graphicsOverlay;

    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;
    private static final boolean useAutoFocus = true;
    private static final boolean useFlash = true;
    private Vibrator vibrator;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.barcode_capture);
        this.collectionOfBarcodes = new HashSet<String>();
        vibrator =  (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        cameraSourcePreview = (CameraSourcePreview) findViewById(R.id.preview);
        graphicsOverlay = (GraphicOverlay<BarcodeGraphic>) findViewById(R.id.graphicOverlay);

//        boolean useAutoFocus = getIntent().getBooleanExtra(AUTOFOCUS, false);
//        boolean useFlash = getIntent().getBooleanExtra(USEFLASH, false);

        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(useAutoFocus, useFlash);
        } else {
            requestCameraPermission();
        }

        gestureDetector = new GestureDetector(this, new CaptureGestureListener());
        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

    }

    private void requestCameraPermission() {
        Log.w(TAG, "Camera Permission is not granted. Requesting permission");
        final String[] permission = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permission, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permission, RC_HANDLE_CAMERA_PERM);
            }

        };

        Snackbar.make(graphicsOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();

    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        boolean b = scaleGestureDetector.onTouchEvent(e);

        boolean c = gestureDetector.onTouchEvent(e);

        return b || c || super.onTouchEvent(e);
    }

    @SuppressLint("InLinedApi")
    private void createCameraSource(boolean useAutoFocus, boolean useFlash) {
        Context context = getApplicationContext();
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context)
//                .setBarcodeFormats(Barcode.ISBN)
                .build();
        BarcodeTrackerFactory barcodeTrackerFactory = new BarcodeTrackerFactory(graphicsOverlay);

        barcodeDetector.setProcessor(new MultiProcessor.Builder<>(barcodeTrackerFactory).build());

        if (!barcodeDetector.isOperational()) {
            Log.w(TAG, "Detector Dependencies are not yet available");

            //Detect for lowstorage
            IntentFilter lowStorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowStorageFilter) != null;
            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }

        CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setFacing(com.google.android.gms.vision.CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(useAutoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }
        mCameraSource = builder.setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null).build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    private void startCameraSource() throws SecurityException {
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dig = GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dig.show();
        }

        if (mCameraSource != null) {
            try {
                cameraSourcePreview.start(mCameraSource, graphicsOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    protected void onPause() {
        super.onPause();
        if (cameraSourcePreview != null) {
            cameraSourcePreview.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (cameraSourcePreview != null) {
            cameraSourcePreview.release();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result");
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera resource");
//            boolean autoFocus = getIntent().getBooleanExtra(AUTOFOCUS, false);
//            boolean useFlash = getIntent().getBooleanExtra(USEFLASH, false);
            createCameraSource(useAutoFocus, useFlash);
            return;
        }

        Log.e(TAG, "Permission not granted: results length: " + grantResults.length + " Result code: " + (grantResults.length > 0 ? grantResults[0] : "empty"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Multitracker sample")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();

    }

    private class CaptureGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return onTap(e.getRawX(), e.getRawY()) || super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.e(TAG, "kjdshgfsdf : " + collectionOfBarcodes);
            Intent data = new Intent();
            data.putExtra(BarcodeObject, collectionOfBarcodes);
            setResult(CommonStatusCodes.SUCCESS, data);
            finish();
            return super.onDoubleTap(e);
        }
    }

    /*
    OnTap returns the tapped barcode result to the calling activity
    @return true if the activity is ending
     */
    private boolean onTap(float rawX, float rawY) {
        //Find tap point in the preview frame coordinates
        int[] location = new int[2];
        graphicsOverlay.getLocationOnScreen(location);
        float x = (rawX - location[0]) / graphicsOverlay.getWidthScaleFactor();
        float y = (rawY - location[1]) / graphicsOverlay.getHeightScaleFactor();

        // Find the barcode whose center is closest to the tapped point.
        Barcode best = null;
        float bestDistance = Float.MAX_VALUE;
        for (BarcodeGraphic graphic : graphicsOverlay.getGraphics()) {
            Barcode barcode = graphic.getBarCode();
            if (barcode.getBoundingBox().contains((int) x, (int) y)) {
                // Exact hit, no need to keep looking.
                best = barcode;
                break;
            }
            float dx = x - barcode.getBoundingBox().centerX();
            float dy = y - barcode.getBoundingBox().centerY();
            float distance = (dx * dx) + (dy * dy);  // actually squared distance
            if (distance < bestDistance) {
                best = barcode;
                bestDistance = distance;
            }
        }

        if (best != null) {
            collectionOfBarcodes.add(best.displayValue);
            vibrator.vibrate(50);
            Toast.makeText(this, "Captured " + best.displayValue, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            mCameraSource.doZoom(detector.getScaleFactor());
        }
    }
}