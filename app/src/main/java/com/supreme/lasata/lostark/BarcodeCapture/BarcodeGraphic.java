package com.supreme.lasata.lostark.BarcodeCapture;

/**
 * Created by navee on 01/01/2017.
 */

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.google.android.gms.vision.barcode.Barcode;
import com.supreme.lasata.lostark.BarcodeCapture.camera.GraphicOverlay;

/**
 * Created by lasata on 18/12/2016.
 */
public class BarcodeGraphic extends GraphicOverlay.Graphic{

    private static final int[] COLOR_CHOICES = {
            Color.BLUE,
            Color.CYAN,
            Color.GREEN
    };
    private static int mCurrentColorIndex = 0;
    private Paint rectpaint;
    private Paint textPaint;
    private int mId;

    private Barcode barCode;
    public BarcodeGraphic(GraphicOverlay overLay) {
        super(overLay);
        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        rectpaint = new Paint();
        rectpaint.setColor(selectedColor);
        rectpaint.setStyle(Paint.Style.STROKE);
        rectpaint.setStrokeWidth(4.0f);

        textPaint = new Paint();
        textPaint.setColor(selectedColor);
        textPaint.setTextSize(36.0f);

    }

    public int getId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public Barcode getBarCode() {
        return barCode;
    }

    public void updateItem (Barcode barcode) {
        this.barCode = barcode;
        postInValidate();
    }



    @Override
    public void draw(Canvas canvas) {
        Barcode barcode = barCode;
        if (barcode == null) {

            return;
        }
        RectF rectF = new RectF(barcode.getBoundingBox());
        rectF.left = translateX(rectF.left);
        rectF.top = translateY(rectF.top);
        rectF.right = translateX(rectF.right);
        rectF.bottom = translateY(rectF.bottom);
        canvas.drawText(barcode.rawValue, rectF.left, rectF.bottom, textPaint);

    }
}
