package com.supreme.lasata.lostark.BarcodeCapture.camera;

/**
 * Created by navee on 01/01/2017.
 */

import android.Manifest;
import android.content.Context;
import android.support.annotation.RequiresPermission;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import com.google.android.gms.common.images.Size;

import java.io.IOException;

/**
 * Created by lasata on 18/12/2016.
 */

public class CameraSourcePreview extends ViewGroup {
    private static final int CAMERA_SOURCE_PREVIEW_WIDTH = 320;
    private static final int CAMERA_SOURCE_PREVIEW_HEIGHT = 240;
    private final Context mContext;
    private boolean startRequested;
    private boolean surfaceAvailable;
    private SurfaceView surfaceView;
    private CameraSource cameraSource;
    private GraphicOverlay overLay;

    private static final String TAG = "CameraSourcePreview";
    public CameraSourcePreview(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mContext = context;
        startRequested = false;
        surfaceAvailable = false;

        surfaceView = new SurfaceView(mContext);
        surfaceView.getHolder().addCallback(new SurfaceCallBack());
        addView(surfaceView);
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void start(CameraSource cameraSource) throws IOException, SecurityException{
        if (cameraSource == null) {
            stop();
        }
        this.cameraSource = cameraSource;
        if (this.cameraSource != null) {
            startRequested = true;
            startIfReady();
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void start(CameraSource cameraSource, GraphicOverlay overLay) throws IOException, SecurityException{
        this.overLay = overLay;
        start(cameraSource);
    }

    public void stop() {
        if (cameraSource != null ){
            cameraSource.stop();
        }
    }

    public  void release() {
        if(cameraSource != null) {
            cameraSource.release();
            cameraSource = null;
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int width = CAMERA_SOURCE_PREVIEW_WIDTH;
        int height = CAMERA_SOURCE_PREVIEW_HEIGHT;
        if (cameraSource != null) {
            Size size = cameraSource.getPreviewSize();
            if (size != null) {
                width = size.getWidth();
                height = size.getHeight();
            }
        }

        if (isPotraitMode()) {
            int temp = width;
            width = height;
            height = temp;
        }

        final int layoutWidth = right - left;
        final int layoutHeight = bottom - top;
        // Computes height and width for potentially doing fit width.
        int childWidth = layoutWidth;
        int childHeight = (int)(((float) layoutWidth / (float) width) * height);

        // If height is too tall using fit width, does fit height instead.
        if (childHeight > layoutHeight) {
            childHeight = layoutHeight;
            childWidth = (int)(((float) layoutHeight / (float) height) * width);
        }

        for (int i = 0; i < getChildCount(); ++i) {
            getChildAt(i).layout(0, 0, layoutWidth, layoutHeight);
//            getChildAt(i).layout(0, 0, childWidth, childHeight);
        }

        try {
            startIfReady();
        } catch (SecurityException se) {
            Log.e(TAG,"Do not have permission to start the camera", se);
        } catch (IOException e) {
            Log.e(TAG, "Could not start camera source.", e);
        }
    }

    private class SurfaceCallBack implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            surfaceAvailable = true;
            try {
                startIfReady();
            } catch (IOException e) {
                Log.e(TAG, "Could not start camera source.", e);
            } catch (SecurityException e) {
                Log.e(TAG,"Do not have permission to start the camera", e);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            surfaceAvailable = false;
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    private void startIfReady() throws IOException, SecurityException {
        if (startRequested && surfaceAvailable) {
            cameraSource.start(surfaceView.getHolder());
            if (overLay != null ) {
                Size size = cameraSource.getPreviewSize();
                int min = Math.min(size.getHeight(), size.getWidth());
                int max = Math.max(size.getHeight(), size.getWidth());
                if (isPotraitMode()) {
                    overLay.setCameraInfo(min, max, cameraSource.getCameraFacing());
                } else {
                    overLay.setCameraInfo(max, min, cameraSource.getCameraFacing());
                }
                overLay.clear();
            }
            startRequested = false;
        }
    }

    private boolean isPotraitMode() {
        int orientation = getContext().getResources().getConfiguration().orientation;
        if (orientation == android.content.res.Configuration.ORIENTATION_LANDSCAPE) {
            return false;
        }
        if(orientation == android.content.res.Configuration.ORIENTATION_PORTRAIT) {
            return true;
        }
        Log.d(TAG, "Returning false (landscape orientation) by default");
        return false;
    }
}
