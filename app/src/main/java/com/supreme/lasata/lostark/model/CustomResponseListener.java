package com.supreme.lasata.lostark.model;

/**
 * Created by navee on 01/01/2017.
 */

public interface CustomResponseListener<T1> {
    void getResult(T1 object);
}
