package com.supreme.lasata.lostark.BarcodeCapture;

/**
 * Created by navee on 01/01/2017.
 */

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.supreme.lasata.lostark.BarcodeCapture.camera.GraphicOverlay;

/**
 * Created by lasata on 18/12/2016.
 */
public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    public BarcodeTrackerFactory(GraphicOverlay<BarcodeGraphic> graphicsOverlay) {
        mGraphicOverlay = graphicsOverlay;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        BarcodeGraphic barcodeGraphic = new BarcodeGraphic(mGraphicOverlay);
        return new BarcodeGraphicTracker(mGraphicOverlay, barcodeGraphic);
    }
}
