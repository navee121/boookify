package com.supreme.lasata.lostark.model;

/**
 * Created by navee on 01/01/2017.
 */


import java.util.ArrayList;
import java.util.List;

/**
 * Created by lasata on 17/12/2016.
 */

public class Book {
    private String name;
    private String description;
    private String bookCoverUrl;
    private List<String> authorNames;
    private String ISBN;

    public Book() {
        super();
        this.authorNames = new ArrayList<>();
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getBookCover() {
        return bookCoverUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBookCover(String bookCoverUrl) {
        this.bookCoverUrl = bookCoverUrl;
    }

    public String getName() {
        return name;
    }

    public List<String> getAuthorNames() {
        return authorNames;
    }

    public void setAuthorNames(List<String> authorNames) {
        this.authorNames = authorNames;
    }

    public String getDescription() {
        return description;
    }

}
