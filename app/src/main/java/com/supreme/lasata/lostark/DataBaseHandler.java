package com.supreme.lasata.lostark;

/**
 * Created by navee on 01/01/2017.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.supreme.lasata.lostark.model.Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by navee on 30/12/2016.
 */

public class DataBaseHandler extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "bookDb";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "books";
    private static final String COLUMN_BOOK_NAME = "book_NAME";
    private static final String COLUMN_BOOK_DESC = "book_DESC";
    private static final String COLUMN_BOOK_COVER_URL = "book_COVER_URL";
    private static final String COLUMN_BOOK_ISBN = "book_ISBN";
    private static final String COLUMN_BOOK_AUTHOR = "book_AUTHOR";

    private static DataBaseHandler instance = null;
    public static DataBaseHandler getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseHandler(context);
        }
        return instance;
    }
    public static DataBaseHandler getInstance() {
        if (instance == null) {
            throw new IllegalStateException(DataBaseHandler.class.getSimpleName() + " is not initialized. Call " +
                    "call getInstance(context) first");
        }
        return instance;
    }


    private DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        context.deleteDatabase(DATABASE_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_BOOK_NAME + " TEXT,"
                + COLUMN_BOOK_DESC + " TEXT,"
                + COLUMN_BOOK_COVER_URL + " TEXT,"
                + COLUMN_BOOK_AUTHOR + " TEXT,"
                + COLUMN_BOOK_ISBN + " TEXT PRIMARY KEY"
                + ")";
        db.execSQL(CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // Create tables again
        onCreate(db);
    }

    public void addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_BOOK_NAME, book.getName());
        contentValues.put(COLUMN_BOOK_DESC, book.getDescription());
        contentValues.put(COLUMN_BOOK_COVER_URL, book.getBookCover());
        contentValues.put(COLUMN_BOOK_AUTHOR, book.getAuthorNames().toString());
        contentValues.put(COLUMN_BOOK_ISBN, book.getISBN());


        db.insert(TABLE_NAME, null, contentValues);
        db.close();
    }

    public Book getBook(String isbn) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from " + TABLE_NAME + " where " + COLUMN_BOOK_ISBN + "=" + isbn + "", null );

        if (cursor != null) {
            cursor.moveToFirst();
        }
        Book book = new Book();
        book.setName(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_NAME)));
        book.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_DESC)));
        book.setISBN(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_ISBN)));
        book.setAuthorNames(Arrays.asList(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_AUTHOR)).split("\\s*,\\s*")));
        book.setBookCover(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_COVER_URL)));
        return book;
    }

    public List<Book> getAllBooks() {
        List<Book> bookList = new ArrayList<Book>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Book book = getBook(cursor.getString(cursor.getColumnIndex(COLUMN_BOOK_ISBN)));
                bookList.add(book);
            } while (cursor.moveToNext());
        }
        return bookList;
    }

    public int getBooksCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.close();
        return cursor.getCount();
    }
    public int upDateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_BOOK_NAME, book.getName());
        values.put(COLUMN_BOOK_DESC, book.getDescription());
        values.put(COLUMN_BOOK_AUTHOR, book.getAuthorNames().toString());
        values.put(COLUMN_BOOK_COVER_URL, book.getBookCover());

        return db.update(TABLE_NAME, values, COLUMN_BOOK_ISBN + " =? ", new String[]{String.valueOf(book.getISBN())});


    }
    public void deleteBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_BOOK_ISBN + "=?", new String[] {String.valueOf(book.getISBN())});
        db.close();
    }

}
