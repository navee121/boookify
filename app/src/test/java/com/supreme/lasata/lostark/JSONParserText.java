package com.supreme.lasata.lostark;


import com.supreme.lasata.lostark.model.Book;
import com.supreme.lasata.lostark.model.JSONParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by navee on 01/01/2017.
 */

public class JSONParserText {
    String jsonData = "{\n" +
            " \"kind\": \"books#volumes\",\n" +
            " \"totalItems\": 1,\n" +
            " \"items\": [\n" +
            "  {\n" +
            "   \"kind\": \"books#volume\",\n" +
            "   \"id\": \"vHlTOVTKHeUC\",\n" +
            "   \"etag\": \"j6PhYdaZzRA\",\n" +
            "   \"selfLink\": \"https://www.googleapis.com/books/v1/volumes/vHlTOVTKHeUC\",\n" +
            "   \"volumeInfo\": {\n" +
            "    \"title\": \"How Google Tests Software\",\n" +
            "    \"authors\": [\n" +
            "     \"James A. Whittaker\",\n" +
            "     \"Jason Arbon\",\n" +
            "     \"Jeff Carollo\"\n" +
            "    ],\n" +
            "    \"publisher\": \"Addison-Wesley Professional\",\n" +
            "    \"publishedDate\": \"2012\",\n" +
            "    \"description\": \"Describes the techniques Google uses to test their software, and offers similiar techniques for analyzing risk and planning tests, allowing an Internet company to become more productive.\",\n" +
            "    \"industryIdentifiers\": [\n" +
            "     {\n" +
            "      \"type\": \"ISBN_13\",\n" +
            "      \"identifier\": \"9780321803023\"\n" +
            "     },\n" +
            "     {\n" +
            "      \"type\": \"ISBN_10\",\n" +
            "      \"identifier\": \"0321803027\"\n" +
            "     }\n" +
            "    ],\n" +
            "    \"readingModes\": {\n" +
            "     \"text\": false,\n" +
            "     \"image\": true\n" +
            "    },\n" +
            "    \"pageCount\": 281,\n" +
            "    \"printType\": \"BOOK\",\n" +
            "    \"categories\": [\n" +
            "     \"Computers\"\n" +
            "    ],\n" +
            "    \"averageRating\": 5.0,\n" +
            "    \"ratingsCount\": 2,\n" +
            "    \"maturityRating\": \"NOT_MATURE\",\n" +
            "    \"allowAnonLogging\": false,\n" +
            "    \"contentVersion\": \"preview-1.0.0\",\n" +
            "    \"imageLinks\": {\n" +
            "     \"smallThumbnail\": \"http://books.google.com/books/content?id=vHlTOVTKHeUC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api\",\n" +
            "     \"thumbnail\": \"http://books.google.com/books/content?id=vHlTOVTKHeUC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api\"\n" +
            "    },\n" +
            "    \"language\": \"en\",\n" +
            "    \"previewLink\": \"http://books.google.co.uk/books?id=vHlTOVTKHeUC&printsec=frontcover&dq=isbn:9780321803023&hl=&cd=1&source=gbs_api\",\n" +
            "    \"infoLink\": \"http://books.google.co.uk/books?id=vHlTOVTKHeUC&dq=isbn:9780321803023&hl=&source=gbs_api\",\n" +
            "    \"canonicalVolumeLink\": \"http://books.google.co.uk/books/about/How_Google_Tests_Software.html?hl=&id=vHlTOVTKHeUC\"\n" +
            "   },\n" +
            "   \"saleInfo\": {\n" +
            "    \"country\": \"GB\",\n" +
            "    \"saleability\": \"NOT_FOR_SALE\",\n" +
            "    \"isEbook\": false\n" +
            "   },\n" +
            "   \"accessInfo\": {\n" +
            "    \"country\": \"GB\",\n" +
            "    \"viewability\": \"PARTIAL\",\n" +
            "    \"embeddable\": true,\n" +
            "    \"publicDomain\": false,\n" +
            "    \"textToSpeechPermission\": \"ALLOWED_FOR_ACCESSIBILITY\",\n" +
            "    \"epub\": {\n" +
            "     \"isAvailable\": false\n" +
            "    },\n" +
            "    \"pdf\": {\n" +
            "     \"isAvailable\": false\n" +
            "    },\n" +
            "    \"webReaderLink\": \"http://books.google.co.uk/books/reader?id=vHlTOVTKHeUC&hl=&printsec=frontcover&output=reader&source=gbs_api\",\n" +
            "    \"accessViewStatus\": \"SAMPLE\",\n" +
            "    \"quoteSharingAllowed\": false\n" +
            "   },\n" +
            "   \"searchInfo\": {\n" +
            "    \"textSnippet\": \"Describes the techniques Google uses to test their software, and offers similiar techniques for analyzing risk and planning tests, allowing an Internet company to become more productive.\"\n" +
            "   }\n" +
            "  }\n" +
            " ]\n" +
            "}";

    @Test
    public void shouldGetCorrectAuthorData() throws JSONException {
        JSONParser jsonParser = JSONParser.getInstance();
        Book book = jsonParser.parseJSONtoBook(new JSONObject(jsonData), "9780321803023");
        Assert.assertEquals("How Google Tests Software", book.getName());
        List<String> expectedAuthors = new ArrayList<String>();
        expectedAuthors.add("James A. Whittaker");
        expectedAuthors.add("James A. Whittaker");
        expectedAuthors.add("Jeff Carollo");



        Assert.assertEquals(expectedAuthors, book.getAuthorNames());
        String expectedImageLink = "http://books.google.com/books/content?id=vHlTOVTKHeUC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api";
        Assert.assertEquals(expectedImageLink, book.getBookCover());
    }
}
